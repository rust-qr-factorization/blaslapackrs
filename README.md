# blaslapackrs

BLAS/LAPACK in pure rust using ndarray. Currently the bare minimum needed to support a sparse QR factorization.